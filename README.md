# streamlabs-windows95

Custom Donation Bar with Windows 95 Style!

---

This little progressbar is what I use for my Streamlabs "Donation Goal" and "Follower Goal" widgets.
You may use this bar for yourself, it would be cool if you credited me.

## Instructions:

Here's how to use it:

- Login to your Streamlabs account at `https://streamlabs.com/dashboard`.
- Select "All Widgets" from the left menu
- Go to either the "Donation Goal" or "Follower Goal" widget
- Next to the "Manage Goal" tab you'll find "Settings". Click it.
- All the way down below, tick "Enabled" for the "Enable Custom HTML/CSS" option.
- Paste the HTML in the HTML field. (Check the comments in the code).
- Paste the CSS in the CSS field.
- Paste the JavaScript in the JS field.
- Everything should now work!

### TODO

Need to add Windows 95 font somewhere to host it.