// Events will be sent when someone followers
// Please use event listeners to run functions.

const calcPerc = (current, total) => {
    return (current * 100) / total;
};

document.addEventListener('goalLoad', function (obj) {
    // obj.detail will contain information about the current goal
    // this will fire only once when the widget loads
    console.log(obj.detail);
    $('#title').text(obj.detail.title);
    $('#goal-current').text(obj.detail.amount.current);
    let widthValue = calcPerc(obj.detail.amount.current, obj.detail.amount.target) + '%';
    $('#goal-progress').css('width', widthValue);
    $('#goal-total').text(obj.detail.amount.target);
    // $('#goal-end-date').text(obj.detail.to_go.ends_at);
});

document.addEventListener('goalEvent', function (obj) {
    // obj.detail will contain information about the goal
    console.log(obj.detail);
    $('#goal-current').text(obj.detail.amount.current);
});


